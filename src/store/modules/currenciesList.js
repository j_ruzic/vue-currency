const state = {
    currenciesList: [],
    selectedCurrency: "",
    searchTerm: "",
    pageType: ""
};

const getters = {
    allCurrencies: state => state.currenciesList,
    filteredCurrencies: state => state.currenciesList.filter(item => {
        return state.searchTerm.length == 0 || item.iso.includes(state.searchTerm) || item.symbol.includes(state.searchTerm)
    }),
    selectedCurrencyItem: state => state.selectedCurrency,
    pageType: state => state.pageType
};

const actions = {
    handleCurrency: (context, payload) => {
        context.commit("HANDLE_CURRENCY", payload);
    },
    deleteCurrency: (context, payload) => {
        context.commit("DELETE_CURRENCY", payload);
    },
    selectedCurrency: (context, payload) => {
        context.commit("SELECTED_CURRENCY", payload);
    },
    searchCurrency: (context, payload) => {
        context.commit("SEARCH_CURRENCY", payload);
    },
    setPageType: (context, payload) => {
        context.commit("SET_PAGE_TYPE", payload);
    }
};

const mutations = {
    HANDLE_CURRENCY: (state, payload) => {
        if(state.pageType == "Add") {
            state.currenciesList.unshift(payload.newCurrency);
        } else if(state.pageType == "Edit") {
            state.currenciesList.forEach(currency => {
                if(currency.iso == payload.selectedCurrencyItem.iso) {
                    currency.id = payload.newCurrency.id;
                    currency.iso = payload.newCurrency.iso;
                    currency.symbol = payload.newCurrency.symbol;
                }
            });
        }
    },
    DELETE_CURRENCY: (state, payload) => {
        const index = state.currenciesList.findIndex(currency => currency.id == payload);
        state.currenciesList.splice(index, 1);
    },
    SELECTED_CURRENCY: (state, payload) => {
        state.selectedCurrency = payload.currency;
        state.pageType = payload.pageType;
    },
    SEARCH_CURRENCY: (state, payload) => {
        state.searchTerm = payload;
    },
    SET_PAGE_TYPE: (state, payload) => {
        if(payload == "Add") state.selectedCurrency = "";
        state.pageType = payload;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}