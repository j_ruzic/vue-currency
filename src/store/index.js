import Vuex from "vuex";
import Vue from "vue";
import VuexPersist from "vuex-persist"
import currenciesList from "./modules/currenciesList";

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
    key: "vuex",
    storage: window.localStorage
});

export default new Vuex.Store({
    modules: {
        currenciesList
    },
    plugins: [vuexPersist.plugin]
});