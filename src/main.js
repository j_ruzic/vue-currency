import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import store from "./store";
import AddAndEditCurrency from "./components/AddAndEditCurrency.vue";

Vue.config.productionTip = false;

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "currencies",
    redirect: "/currencies"
  },
  {
    path: "/currencies/add",
    name: "add",
    component: AddAndEditCurrency,
    props: true
  },
  {
    path: "/currencies/edit/:currencyIso",
    name: "edit",
    component: AddAndEditCurrency
  }
];

const router = new VueRouter({
  routes
});

new Vue({
  store,
  render: h => h(App),
  router
}).$mount("#app");
